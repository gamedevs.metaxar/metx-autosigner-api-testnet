const { createServer } = require('@vercel/node');
const autosignerFunction = require('./autosigner-function');

const server = createServer(autosignerFunction);

server.listen(3000, () => {
    console.log('Server listening on port 3000');
  });