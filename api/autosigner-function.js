const { spawn } = require('child_process');

module.exports = async (req, res) => {
  const child = spawn('autosigner.exe', [], {
    detached: true,
    stdio: 'ignore'
  });

  child.unref();

  res.send('Autosigner started successfully!');
};
